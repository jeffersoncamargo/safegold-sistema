function renderCompanies(companies)
{
    if (!companies.length) {
        paginate = 0;
    }

    for (i in companies) {
        html = ''
        html += '<div class="blocos-destaques wow fadeInUp">'
        html += '<div class="campos-empresa">'
        html += '<ul class="col-esq">'
        html += '<li class="txt-medium">Código</li>'
        html += '<li class="txt-medium">Segmento</li>'
        html += '<li class="txt-medium">Faturamento</li>'
        html += '<li class="txt-medium">Nº Funcionários</li>'
        html += '<li class="txt-medium">Cidade/Estado</li>'
        html += '</ul>'
        html += '<ul class="col-dir">'
        html += '<li class="txt-medium"><strong>' + companies[i].code + '</strong></li>'
        html += '<li class="txt-medium">' + companies[i].segment + '</li>'
        html += '<li class="txt-medium"><img src="img/icone-cadeado.png" alt=""></li>'
        html += '<li class="txt-medium"><img src="img/icone-cadeado.png" alt=""></li>'
        html += '<li class="txt-medium">' + companies[i].city + '/' + companies[i].state + '</li>'
        html += '</ul>'
        html += '</div>'
        html += '<div class="empresa-destaque">'
        html += '<p class="txt-medium">Empresa <i><img src="img/icone-cadeado.png" alt=""></i></p>'
        html += '<div class="img-destaque">'
        html += '<img src="' + companies[i].image + '" alt="">'
        html += '<div class="codigo-txt">' + companies[i].code + '</div>'
        html += '</div>'
        html += '</div>'
        html += '<div class="bt-padrao">'
        html += '<a href="#login" class="txt teaser_link" data-toggle="modal" data-target="#login" data-id="' + companies[i].id + '">VER TEASER</a>'
        html += '</div>'
        html += '</div>'

        $('#lista-empresas').append(html)
    }

    $('.teaser_link').click(function () {
        company_id = $(this).data('id')
    })
}

function getCompanies()
{
    getting = 1;

    var filters = {}
    for (i in filtros) {
        filters[i] = filtros[i]
    }
    filters['page'] = page;

    $.ajax({
        method: 'GET',
        url: baseUrl + 'companies',
        data: filters,
        dataType: 'json',
        success: function (res) {
            renderCompanies(res.data);
            getting = 0;
        }
    });
}

function removeFilter(id, type)
{
    switch (type) {
        case 'segment':
            $('[name="segment_id"]:checked').removeProp('checked');
            break;
        case 'earning_range':
            $('[name="earning_range_id"]:checked').removeProp('checked');
            break;
        case 'state':
            $('#lista-states input[value="' + id + '"]').removeProp('checked');
            break;
    }

    renderFiltros()
}

function renderFiltros()
{
    filtros = [];
    $('.links-filtro ul').html('');

    if ($('[name="segment_id"]:checked').length) {
        el = $('[name="segment_id"]:checked')
        filtros['segment_id'] = el.prop('value')
        html = '<li> ' + el.parent().find('label').text() + '<i data-type="segment" data-id="' + el.prop('value') + '" style="cursor: pointer"><img src="img/icone-fechar.png" alt=""></i></li>'
        $('.links-filtro ul').append(html)
    }

    if ($('[name="earning_range_id"]:checked').length) {
        el = $('[name="earning_range_id"]:checked')
        filtros['earning_range_id'] = el.prop('value')
        html = '<li> ' + el.parent().find('label').text() + '<i data-type="earning_range" data-id="' + el.prop('value') + '" style="cursor: pointer"><img src="img/icone-fechar.png" alt=""></i></li>'
        $('.links-filtro ul').append(html)
    }

    if ($('#lista-states input:checked').length) {
        filtros['state'] = []
        $('#lista-states input:checked').each(function() {
            el = $(this)
            filtros['state'].push(el.prop('value'))
            html = '<li> ' + el.parent().find('label').text() + '<i data-type="state" data-id="' + el.prop('value') + '" style="cursor: pointer"><img src="img/icone-fechar.png" alt=""></i></li>'
            $('.links-filtro ul').append(html)
        })
    }

    $('.links-filtro ul i').click(function() {
        removeFilter($(this).data('id'), $(this).data('type'));
    });

    paginate = 1;
    page = 1;
    $('#lista-empresas').html('');
    getCompanies();
}

function renderSegments(segments)
{
    html = ''

    for (i in segments) {
        html += '<li>'
        html += '<input type="radio" id="segment-' + segments[i].id + '" name="segment_id" value="' + segments[i].id + '">'
        html += '<label for="segment-' + segments[i].id + '">' + segments[i].name + '</label>'
        html += '</li>'
    }

    $('#lista-segmentos').html(html);
    $('#lista-segmentos input').click(function() {
        renderFiltros();
    });
}

function renderRanges(ranges)
{
    html = ''

    for (i in ranges) {
        html += '<li>'
        html += '<input type="radio" id="range-' + i + '" name="earning_range_id" value="' + i + '">'
        html += '<label for="range-' + i + '">' + ranges[i] + '</label>'
        html += '</li>'
    }

    $('#lista-ranges').html(html);
    $('#lista-ranges input').click(function() {
        renderFiltros();
    });
}

function login ()
{
    if (posting) {
        return;
    }

    $('#error-login-email').text('');
    $('#error-login-password').text('');

    var email = $('#login-email').val();
    var password = $('#login-password').val();

    if (!email) {
        $('#error-login-email').text('Obrigatório');
        return;
    }
    if (!password) {
        $('#error-login-password').text('Obrigatório');
        return;
    }

    posting = 1

    $.ajax({
        method: 'POST',
        url: baseUrl + 'login',
        data: {
            email: email,
            password: password,
            client_id: 2,
            client_secret: 'AaQs6pqnMTHpRVXeb7IPIVUVKhPBHEwx19ttLNkU',
            grant_type: 'password'
        },
        dataType: 'json',
        success: function (res) {
            posting = 0
            token = res.data.token;
            $('#login').modal('hide');
            pdf()
        },
        error: function (res) {
            posting = 0
            $('#error-login-email').text('E-mail e/ou senha não correspondem');
        }
    });
}

function pdf()
{
    $.ajax({
        method: 'GET',
        url: baseUrl + 'customer',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + token);
        },
        dataType: 'json',
        success: function (res) {
            token = ''
            $('#pdf').modal('show')
            easyPDF(res.data.data, 'Teaser')
            
        },
        error: function (res) {
            console.log(res)
        }
    });
}

function register ()
{
    if (posting) {
        return;
    }

    $('#error-register-email').text('');
    $('#error-register-name').text('');
    $('#error-register-cpf_cnpj').text('');
    $('#error-register-phone').text('');

    var email = $('#register-email').val();
    var name = $('#register-name').val();
    var cpf_cnpj = $('#register-cpf_cnpj').val();
    var phone = $('#register-phone').val();
    var indication = $('#register-indication').val();

    if (!name) {
        $('#error-register-name').text('Obrigatório');
        return;
    }
    if (!email) {
        $('#error-register-email').text('Obrigatório');
        return;
    }
    if (!phone) {
        $('#error-register-phone').text('Obrigatório');
        return;
    } else if (phone.length < 14) {
        $('#error-register-phone').text('Telefone inválido');
        return;
    }
    if (!cpf_cnpj) {
        $('#error-register-cpf_cnpj').text('Obrigatório');
        return;
    } else if (cpf_cnpj.length != 14 && cpf_cnpj.length != 18) {
        $('#error-register-cpf_cnpj').text('CPF/CNPJ inválido');
        return;
    }

    posting = 1

    $.ajax({
        method: 'POST',
        url: baseUrl + 'register',
        data: {
            company_id: company_id,
            name: name,
            cpf_cnpj: cpf_cnpj,
            email: email,
            phone: phone,
            indication: indication
        },
        dataType: 'json',
        success: function (res) {
            posting = 0
            token = res.data.token;
            $('#cadastro').modal('hide');
            $('#login').modal('hide');
            $('#cadastro-ok').modal('show');
        },
        error: function (res) {
            posting = 0
            $('#error-register-name').text(res.responseJSON.message);
        }
    });
}

function forgot ()
{
    if (posting) {
        return;
    }

    $('#error-forgot-email').text('');
    $('#ok-forgot-email').text('');

    var email = $('#forgot-email').val();

    if (!email) {
        $('#error-forgot-email').text('Obrigatório');
        return;
    }

    posting = 1

    $.ajax({
        method: 'POST',
        url: baseUrl + 'forgot-password',
        data: {
            company_id: company_id,
            email: email
        },
        dataType: 'json',
        success: function (res) {
            posting = 0
            $('#ok-forgot-email').text('Recebemos sua solicitação. Caso seu email pertença a um cadastro válido e autorizado, você receberá uma nova senha.')
        },
        error: function (res) {
            posting = 0
            $('#ok-forgot-email').text('Recebemos sua solicitação. Caso seu email pertença a um cadastro válido e autorizado, você receberá uma nova senha.');
        }
    });
}

var baseUrl = 'https://safegold.com.br/admin/api/';
// var baseUrl = 'http://127.0.0.1:8000/api/';

var filtros = [];
var page = 1;
var paginate = 1;
var getting = 0;
var token = '';
var company_id = 0;
var posting = 0;

$(function() {
    $.get(baseUrl + 'segments', function (res) {
        renderSegments(res.data);
    });

    $.get(baseUrl + 'earning-ranges', function (res) {
        renderRanges(res.data);
    });

    $('#lista-states input').click(function() {
        renderFiltros();
    });

    renderFiltros();

    $(window).scroll(function () {
        if (window.scrollY >= $('footer').offset().top - 500) {
            if (paginate && !getting) {
                page++;
                getCompanies();
            }
        }
    })

    if (window.location.hash == '#login') {
        $('#login').modal('show')
    }

    $('#esqueci-minha-senha').on('show.bs.modal', function () {
        $('#cadastro').modal('hide');
    })

    $('#pdf').on('hide.bs.modal', function () {
        $('#pdfDialog').remove();
    })

    $('#bt-login').click(function() {
        login()
    })

    $('#login-email').keyup(function(e) {
        if (e.keyCode == 13) {
            login()
        }
    })

    $('#login-password').keyup(function(e) {
        if (e.keyCode == 13) {
            login()
        }
    })

    $('#bt-register').click(function() {
        register()
    })

    $('#register-cpf_cnpj').keyup(function(e) {
        if (e.keyCode == 13) {
            register()
        }
    })

    $('#register-name').keyup(function(e) {
        if (e.keyCode == 13) {
            register()
        }
    })

    $('#register-email').keyup(function(e) {
        if (e.keyCode == 13) {
            register()
        }
    })

    $('#register-phone').keyup(function(e) {
        if (e.keyCode == 13) {
            register()
        }
    })

    $('#register-indication').keyup(function(e) {
        if (e.keyCode == 13) {
            register()
        }
    })

    var cpfCnpjMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length > 11 ? '00.000.000/0000-00' : '000.000.000-009';
    },
    cpfCnpjOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(cpfCnpjMaskBehavior.apply({}, arguments), options);
        }
    };
    $('#register-cpf_cnpj').mask(cpfCnpjMaskBehavior, cpfCnpjOptions);

    var SPMaskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

    $('#register-phone').mask(SPMaskBehavior, spOptions);

    $('#bt-forgot').click(function() {
        forgot()
    })

    $('#forgot-email').keyup(function(e) {
        if (e.keyCode == 13) {
            forgot()
        }
    })
})