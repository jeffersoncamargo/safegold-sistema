$( document ).ready(function() {

	// WOW
	wow = new WOW({
		live: true,
	}).init();

	//MENU MOBILE
    $('.drawer').drawer();

    //barraFixa
    var navTop = $('.bt-whats');
        $(window).scroll(function () {
            if ($(this).scrollTop() > 150) {
                navTop.addClass("fixedBottom");
            } else {
                navTop.removeClass("fixedBottom");
        }
    });
    
	// REPLACE ALL SVG IMAGES WITH INLINE SVG
	jQuery('img.svg').each(function(){
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');
		jQuery.get(imgURL, function(data) {
			var $svg = jQuery(data).find('svg');
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
			}
			$svg = $svg.removeAttr('xmlns:a');
			$img.replaceWith($svg);
		}, 'xml');
	});

	// $(".btt").toggle(function () {
	// 	$(".lista-filtro").removeClass("active");
	// 	$(".lista-filtro").addClass("active");
	// });

	$(".bt-seg .btt").click(function(){
		$(".lista-filtro").removeClass("active");
		$(".bt-seg .lista-filtro").toggleClass("active");
	});
	$(".bt-fat .btt").click(function(){
		$(".lista-filtro").removeClass("active");
		$(".bt-fat .lista-filtro").toggleClass("active");
	});
	$(".bt-est .btt").click(function(){
		$(".lista-filtro").removeClass("active");
		$(".bt-est .lista-filtro").toggleClass("active");
	});
	$(".lista-filtro .header").click(function(){
		$(".lista-filtro").removeClass("active");
	});


	$('#myModal').on('shown.bs.modal', function () {
		$('#myInput').trigger('focus')
	});
	$('#modal-video1').on('hidden.bs.modal', function () {
        $("#modal-video1").attr("src", $("#modal-video1").attr("src"));
    });
});

