<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Setup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('role_id')->unsigned();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('active')->default(true);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('role_id')->references('id')->on('roles');
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
        
        Schema::create('failed_jobs', function (Blueprint $table) {
            $table->id();
            $table->text('connection');
            $table->text('queue');
            $table->longText('payload');
            $table->longText('exception');
            $table->timestamp('failed_at')->useCurrent();
        });

        Schema::create('segments', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('cropinfo')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('earning_ranges', function (Blueprint $table) {
            $table->id();
            $table->decimal('min', 12, 2);
            $table->decimal('max', 12, 2);
            $table->timestamps();
        });

        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('segment_id')->unsigned();
            $table->bigInteger('earning_range_id')->unsigned();
            $table->string('code')->unique();
            $table->string('name');
            $table->integer('employeees');
            $table->string('state', 2)->index();
            $table->string('city');
            $table->string('cropinfo')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('segment_id')->references('id')->on('segments');
            $table->foreign('earning_range_id')->references('id')->on('earning_ranges');
        });

        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id')->unsigned();
            $table->bigInteger('registering_user_id')->unsigned()->nullable();
            $table->bigInteger('approving_user_id')->unsigned()->nullable();
            $table->string('cpf_cnpj');
            $table->text('cpf_cnpj_hash');
            $table->string('cpf_cnpj_mask');
            $table->string('password')->nullable();
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('indication')->nullable();
            $table->string('ip')->nullable();
            $table->string('browser')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->boolean('active')->default(true);
            $table->datetime('registration_at')->nullable();
            $table->datetime('approbation_at')->nullable();
            $table->datetime('valid_until')->nullable();
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('registering_user_id')->references('id')->on('users');
            $table->foreign('approving_user_id')->references('id')->on('users');
        });

        Schema::create('accesses', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id')->unsigned();
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failed_jobs');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('users');
    }
}
