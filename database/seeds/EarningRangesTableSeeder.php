<?php

use Illuminate\Database\Seeder;

class EarningRangesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('earning_ranges')->insert([
            'min' => 1000000,
            'max' => 10000000,
        ]);
        DB::table('earning_ranges')->insert([
            'min' => 10000001,
            'max' => 50000000,
        ]);
        DB::table('earning_ranges')->insert([
            'min' => 50000001,
            'max' => 100000000,
        ]);
        DB::table('earning_ranges')->insert([
            'min' => 100000001,
            'max' => 200000000,
        ]);
        DB::table('earning_ranges')->insert([
            'min' => 200000001,
            'max' => 400000000,
        ]);
        DB::table('earning_ranges')->insert([
            'min' => 400000001,
            'max' => 600000000,
        ]);
        DB::table('earning_ranges')->insert([
            'min' => 600000001,
            'max' => 2000000000,
        ]);
    }
}
