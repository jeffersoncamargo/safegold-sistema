<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Admin',
        ]);
        DB::table('roles')->insert([
            'name' => 'Gerente',
        ]);
        DB::table('roles')->insert([
            'name' => 'Consultor',
        ]);

        DB::table('users')->insert([
            'role_id' => 1,
            'name' => 'Admin',
            'email' => 'jeffersonflavio.camargo@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('7812738'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('users')->insert([
            'role_id' => 1,
            'name' => 'Pam',
            'email' => 'pamelajuliao@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('p@Z0Ak3J8KXP'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
