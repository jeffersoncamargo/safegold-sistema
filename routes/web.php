<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if (Auth::user()) {
		return redirect(route('home'));
	}
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('users', 'UsersController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

	Route::resource('segments', 'SegmentsController', ['except' => ['show']]);
	Route::resource('companies', 'CompaniesController', ['except' => ['show']]);
	Route::resource('customers', 'CustomersController', ['except' => ['show']]);
	Route::get('customers/approving-list', ['as' => 'customers.approving-list', 'uses' => 'CustomersController@approvingList']);
	Route::get('customers/{customer}/approve', ['as' => 'customers.approve', 'uses' => 'CustomersController@approve']);
	Route::get('customers/{customer}/repprove', ['as' => 'customers.repprove', 'uses' => 'CustomersController@repprove']);
});

