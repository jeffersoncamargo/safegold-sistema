<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Api\RegisterController@register');
Route::post('login', 'Api\RegisterController@login');
Route::post('forgot-password', 'Api\RegisterController@forgotPassword');

Route::get('segments', 'Api\SegmentsController@index');
Route::get('earning-ranges', 'Api\EarningRangesController@index');
Route::get('companies', 'Api\CompaniesController@index');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('customer', 'Api\CustomersController@show');
});
