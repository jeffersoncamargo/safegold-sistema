@extends('layouts.app', ['activePage' => 'customers', 'titlePage' => __('Novo cliente')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="{{ route('customers.store') }}" autocomplete="off" class="form-horizontal">
                    @csrf
                    {{-- @method('put') --}}
                    
                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Criar cliente') }}</h4>
                            <p class="card-category">{{ __('Informações do cliente') }}</p>
                        </div>
                        <div class="card-body ">
                            @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Empresa') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('company_id') ? ' has-danger' : '' }}">
                                        <select class="form-control{{ $errors->has('company_id') ? ' is-invalid' : '' }}" name="company_id" id="input-company_id" required="true" aria-required="true">
                                            <option value="">Selecione a empresa...</option>
                                            @foreach(App\Models\Company::where('active', true)->get() as $company)
                                                <option value="{{ $company->id }}" {{ old('company_id', null) == $company->id ? 'selected' : '' }}>{{ $company->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('company_id'))
                                            <span id="company_id-error" class="error text-danger" for="input-company_id">{{ $errors->first('company_id') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('CPF/CNPJ') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('cpf_cnpj') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('cpf_cnpj') ? ' is-invalid' : '' }}" name="cpf_cnpj" id="input-cpf_cnpj" type="text" placeholder="{{ __('CPF/CNPJ') }}" value="{{ old('cpf_cnpj', null) }}" required="true" aria-required="true"/>
                                        @if ($errors->has('cpf_cnpj'))
                                        <span id="cpf_cnpj-error" class="error text-danger" for="input-cpf_cnpj">{{ $errors->first('cpf_cnpj') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Nome') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nome') }}" value="{{ old('name', null) }}" required="true" aria-required="true"/>
                                        @if ($errors->has('name'))
                                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('E-mail') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ __('E-mail') }}" value="{{ old('email', null) }}" required="true" aria-required="true"/>
                                        @if ($errors->has('email'))
                                        <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Telefone') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" id="input-phone" type="text" placeholder="{{ __('Telefone') }}" value="{{ old('phone', null) }}" required="true" aria-required="true"/>
                                        @if ($errors->has('phone'))
                                        <span id="phone-error" class="error text-danger" for="input-phone">{{ $errors->first('phone') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="row">
                                <div class="col-sm-9">
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" name="active" value="0">
                                            <input type="checkbox" name="active" value="1" {{ old('active', null) ? 'checked' : '' }}> Este cliente está ativo?
                                        </label>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">{{ __('Salvar') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        $(function() {
            var cpfCnpjMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length > 11 ? '00.000.000/0000-00' : '000.000.000-009';
            },
            cpfCnpjOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(cpfCnpjMaskBehavior.apply({}, arguments), options);
                }
            };
            $('#input-cpf_cnpj').mask(cpfCnpjMaskBehavior, cpfCnpjOptions);

            var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

            $('#input-phone').mask(SPMaskBehavior, spOptions);
        })
    </script>
@endpush