@extends('layouts.app', ['activePage' => 'customers', 'titlePage' => __('Gerenciar clientes')])

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Clientes</h4>
                        <p class="card-category"> Gerenciamento de clientes para acesso aos teasers</p>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @can('create', App\Models\Customer::class)
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{ route('customers.create') }}" class="btn btn-sm btn-primary">Novo cliente</a>
                                </div>
                            </div>
                        @endcan
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>
                                            Nome
                                        </th>
                                        <th>
                                            CPF
                                        </th>
                                        <th>
                                            E-mail
                                        </th>
                                        <th>
                                            Ativo
                                        </th>
                                        <th>
                                            Aprovado por
                                        </th>
                                        <th>
                                            Aprovado em
                                        </th>
                                        <th>
                                            Reprovado por
                                        </th>
                                        <th>
                                            Reprovado em
                                        </th>
                                        <th class="text-right">
                                            Ações
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($customers as $customer)
                                        <tr>
                                            <td>
                                                {{ $customer->name }}
                                            </td>
                                            <td>
                                                {{ strlen($customer->cpf_cnpj_mask) == 11 ? App\Library\Format::cpf($customer->cpf_cnpj_mask) : App\Library\Format::cnpj($customer->cpf_cnpj_mask) }}
                                            </td>
                                            <td>
                                                {{ $customer->email }}
                                            </td>
                                            <td>
                                                {{ $customer->active ? 'Sim' : 'Não' }}
                                            </td>
                                            <td>
                                                {{ $customer->approving_user_id ? $customer->approvingUser->name : '--' }}
                                            </td>
                                            <td>
                                                {{ $customer->approbation_at ? $customer->fmtApprobationAt() : '--' }}
                                            </td>
                                            <td>
                                                {{ $customer->repproving_user_id ? $customer->repprovingUser->name : '--' }}
                                            </td>
                                            <td>
                                                {{ $customer->repprobation_at ? $customer->fmtRepprobationAt() : '--' }}
                                            </td>
                                            <td class="td-actions text-right">
                                                <a rel="tooltip" class="btn btn-primary btn-link" href="{{ route('customers.edit', $customer->id) }}" data-original-title="editar" title="editar">
                                                    <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div>
                                                </a>
                                                @if (!$customer->approbation_at)
                                                    <a rel="tooltip" class="btn btn-success btn-link confirm" href="{{ route('customers.approve', $customer->id) }}" data-original-title="aprovar" title="aprovar" data-confirmation="Tem certeza que deseja APROVAR este cadastro?">
                                                        <i class="material-icons">thumb_up</i>
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                @endif
                                                @if (!$customer->repprobation_at)
                                                    <a rel="tooltip" class="btn btn-danger btn-link confirm" href="{{ route('customers.repprove', $customer->id) }}" data-original-title="reprovar" title="reprovar" data-confirmation="Tem certeza que deseja REPROVAR este cadastro?">
                                                        <i class="material-icons">thumb_down</i>
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-12 text-right">
                                {{ $customers->appends(request()->except(['page','_token']))->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @endsection