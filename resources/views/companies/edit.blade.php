@extends('layouts.app', ['activePage' => 'companies', 'titlePage' => __('Editar empresa')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="{{ route('companies.update', $company->id) }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    
                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ $company->name }}</h4>
                            <p class="card-category">{{ __('Informações da empresa') }}</p>
                        </div>
                        <div class="card-body ">
                            @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Segmento') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('segment_id') ? ' has-danger' : '' }}">
                                        <select class="form-control{{ $errors->has('segment_id') ? ' is-invalid' : '' }}" name="segment_id" id="input-segment_id" required="true" aria-required="true">
                                            <option value="">Selecione o segmento...</option>
                                            @foreach(App\Models\Segment::where('active', true)->get() as $segment)
                                                <option value="{{ $segment->id }}" {{ old('segment_id', $company->segment_id) == $segment->id ? 'selected' : '' }}>{{ $segment->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('segment_id'))
                                            <span id="segment_id-error" class="error text-danger" for="input-segment_id">{{ $errors->first('segment_id') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Faturamento') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('earning_range_id') ? ' has-danger' : '' }}">
                                        <select class="form-control{{ $errors->has('earning_range_id') ? ' is-invalid' : '' }}" name="earning_range_id" id="input-earning_range_id" required="true" aria-required="true">
                                            <option value="">Selecione o faturamento...</option>
                                            @foreach(App\Models\EarningRange::forSelect() as $id => $range)
                                                <option value="{{ $id }}" {{ old('earning_range_id', $company->earning_range_id) == $id ? 'selected' : '' }}>{{ $range }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('earning_range_id'))
                                            <span id="earning_range_id-error" class="error text-danger" for="input-earning_range_id">{{ $errors->first('earning_range_id') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Código') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('code') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" id="input-code" type="text" placeholder="{{ __('Código') }}" value="{{ old('code', $company->code) }}" required="true" aria-required="true"/>
                                        @if ($errors->has('code'))
                                            <span id="code-error" class="error text-danger" for="input-code">{{ $errors->first('code') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Nome') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nome') }}" value="{{ old('name', $company->name) }}" required="true" aria-required="true"/>
                                        @if ($errors->has('name'))
                                            <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Funcionários') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('employeees') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('employeees') ? ' is-invalid' : '' }}" name="employeees" id="input-employeees" type="text" placeholder="{{ __('Funcionários') }}" value="{{ old('employeees', $company->employeees) }}" required="true" aria-required="true"/>
                                        @if ($errors->has('employeees'))
                                            <span id="employeees-error" class="error text-danger" for="input-employeees">{{ $errors->first('employeees') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Estado') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('state') ? ' has-danger' : '' }}">
                                        <select class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" name="state" id="input-state" required="true" aria-required="true">
                                            <option value=""></option>
                                        </select>
                                        @if ($errors->has('state'))
                                            <span id="state-error" class="error text-danger" for="input-state">{{ $errors->first('state') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Cidade') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('city') ? ' has-danger' : '' }}">
                                        <select class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" id="input-city" required="true" aria-required="true">
                                            <option value=""></option>
                                        </select>
                                        @if ($errors->has('city'))
                                            <span id="city-error" class="error text-danger" for="input-city">{{ $errors->first('city') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Teaser') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('file') ? ' has-danger' : '' }}">
                                        <input class="form-control" name="file" id="input-file" type="file" value="{{ old('file', null) }}" style="position: relative; opacity: 1; z-index: 0;" placeholder="Selecione o arquivo" />
                                        <span id="" class="error text-info" for="input-file">Formato: PDF</span>
                                        <br>
                                        @if ($errors->has('file'))
                                            <span id="file-error" class="error text-danger" for="input-file">{{ $errors->first('file') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" name="active" value="0">
                                            <input type="checkbox" name="active" value="1" {{ old('active', $company->active) ? 'checked' : '' }}> Esta empresa está ativa?
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">{{ __('Salvar') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        $(function() {
            new dgCidadesEstados({
                estado: document.getElementById('input-state'),
                estadoVal: '{{ old('state', $company->state) }}',
                cidade: document.getElementById('input-city'),
                cidadeVal: '{{ old('city', $company->city) }}'
            })
        })
    </script>
@endpush