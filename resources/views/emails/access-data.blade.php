<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <title>Safegold</title>
    <style type="text/css">
        body{ margin: 0; padding: 0; }
        img{ border: 0px; display: block; }
    </style>
</head>

<body style="margin: 0 auto; padding: 0; background-color: #fff; width:100%; max-width:600px;" bgcolor="#fff">
    <table style="" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#fff">
        <tbody>
            <tr>
                <td valign="top" align="center">
                    <div style="margin: 0 auto; max-width: 600px;">
                        <table style="width: 100%; max-width: 600px;" width="600" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td valign="top" align="center">
                                      <img style="border: 0px; display: block; width: 100%; " alt="Safegold" width="600" border="0" align="middle" title="Safegold" src="https://safegold.com.br/wp-content/themes/safeGold/assets/img/email-topo.jpg"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    <!-- CONTEUDO -->
    <table style="background-color: #fff; padding:0px 0 30px;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#fff">
        <tbody>
            <tr>
                <td valign="top" align="center">
                    <div style="margin: 0 auto 0px; max-width: 600px;">
                        <table style="width: 100%; background-color: #ffffff;" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                            <tbody>
                                <tr>
                                  	<td style="font-size: 16px; font-family:Arial, Geneva, sans-serif; text-align: center;" valign="top" align="center">
                                        <p style="font-family:Arial, Geneva, sans-serif; color:#000; line-height: 30px; margin-bottom: 30px;">Olá {{ preg_replace('/\s.*/', '', $customer->name) }},</p> 
                                        <p style="font-size:15px; line-height: 25px; color:#000; margin: 0; padding: 0 50px">Você está recebendo esta mensagem porque a Safegold lhe autorizou a visualizar o teaser de uma empresa.</p>
                                        <br>
                                        <p style="font-size:15px; line-height: 25px; color:#000; margin: 0; padding: 0 50px">Seguem os dados:</p>
                                    </td>
                                </tr>	
                                <tr>
                                  	<td style="font-size: 16px; font-family:Arial, Geneva, sans-serif; text-align: left;" valign="top" align="left">		
                                        <ul style="font-size:15px; line-height: 25px; color:#000; margin: 0; padding: 0 50px">
                                            <li><strong>Cód. Empresa:</strong> {{ $customer->company->code }}</li>
                                            <li><strong>Empresa:</strong> {{ $customer->company->name }}</li>
                                            <li><strong>Segmento:</strong> {{ $customer->company->segment->name }}</li>
                                            <li><strong>Seu usuário:</strong> {{ $customer->email }}</li>
                                            <li><strong>Sua senha:</strong> {{ $password }}</li>
                                        </ul>
                                        <br>
                                    </td>
                                </tr>	
                                <tr>
                                  	<td style="font-size: 16px; font-family:Arial, Geneva, sans-serif; text-align: center;" valign="top" align="center">
                                        <p style="font-size:15px; line-height: 25px; color:#000; margin: 0; padding: 0 50px">Para acessar o teaser, <a href="https://safegold.com.br/teasers/#login">clique aqui</a> ou acesse https://safegold.com.br/teasers/#login</p>
                                        <p style="font-size:15px; line-height: 25px; color:#000; margin: 0; padding: 0 50px">Seus dados de acesso são válidos até <strong>{{ $customer->fmtValidUntil() }}</strong>.</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    <!-- RODAPE -->
    <table style="background-color: #fff;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#fff">
        <tbody>
            <tr>
                <td valign="top" align="center">
                    <div style="margin: 0 auto;">
                        <table style="width: 100%;" width="600" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td valign="top">
                                        <div style="display: inline-block; vertical-align: top; width: 100%; ">
                                            <table style="width: 100%;" width="600" cellspacing="0" cellpadding="0" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td width="404" valign="top" align="left">
                                                             <a title="Safegold" target="_blank" style="display: inline-block; float: left;" href="https://safegold.com.br/">
                                                                <img alt="Safegold" title="Safegold" style="border: 0px; display: block; vertical-align: top; margin: 0 auto; width: 404px; height: 102px;" width="404" hspace="0" border="0" align="middle" src="https://safegold.com.br/wp-content/themes/safeGold/assets/img/email-rodape-01.jpg"
                                                                />
                                                            </a>
                                                        </td>
                                                        <td width="29" valign="top" align="left">
                                                             <a title="Safegold" target="_blank" style="display: inline-block; float: left;" href="https://www.facebook.com/safegoldgerenciamento/">
                                                                <img alt="Facebook - Safegold" title="Facebook - Safegold" style="border: 0px; display: block; vertical-align: top; margin: 0 auto; width: 29px; height: 102px;" width="29" hspace="0" border="0" align="middle" src="https://safegold.com.br/wp-content/themes/safeGold/assets/img/email-rodape-02.jpg"
                                                                />
                                                            </a>
                                                        </td>
                                                        <td width="28" valign="top" align="left">
                                                             <a title="Safegold" target="_blank" style="display: inline-block; float: left;" href="https://www.instagram.com/safegold.capital/">
                                                                <img alt="Instagram - Safegold" title="Instagram - Safegold" style="border: 0px; display: block; vertical-align: top; margin: 0 auto; width: 28px; height: 102px;" width="28" hspace="0" border="0" align="middle" src="https://safegold.com.br/wp-content/themes/safeGold/assets/img/email-rodape-03.jpg"
                                                                />
                                                            </a>
                                                        </td>
                                                        <td width="31" valign="top" align="left">
                                                             <a title="Safegold" target="_blank" style="display: inline-block; float: left;" href="https://www.youtube.com/channel/UCnURQZJAHaBVe2pl3qxwMBQ">
                                                                <img alt="YouTube - Safegold" title="YouTube - Safegold" style="border: 0px; display: block; vertical-align: top; margin: 0 auto; width: 31px; height: 102px;" width="31" hspace="0" border="0" align="middle" src="https://safegold.com.br/wp-content/themes/safeGold/assets/img/email-rodape-04.jpg"
                                                                />
                                                            </a>
                                                        </td>
                                                        <td width="32" valign="top" align="left">
                                                             <a title="Safegold" target="_blank" style="display: inline-block; float: left;" href="https://pt.linkedin.com/company/safegold-gerenciamento-de-capital">
                                                                <img alt="Linkedin - Safegold" title="Linkedin - Safegold" style="border: 0px; display: block; vertical-align: top; margin: 0 auto; width: 32px; height: 102px;" width="32" hspace="0" border="0" align="middle" src="https://safegold.com.br/wp-content/themes/safeGold/assets/img/email-rodape-05.jpg"
                                                                />
                                                            </a>
                                                        </td>
                                                        <td width="76" valign="top" align="left">
                                                            <img alt="Safegold" title="Safegold" style="border: 0px; display: block; float: left; vertical-align: top; margin: 0 auto; width: 76px; height: 102px;" width="76" hspace="0" border="0" align="middle" src="https://safegold.com.br/wp-content/themes/safeGold/assets/img/email-rodape-06.jpg"
                                                                />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#fff">
        <tbody>
            <tr>
                <td valign="top" align="center">
                    <div style="margin: 0 auto; max-width: 600px;">
                        <table style="width: 100%; max-width: 600px;" width="600" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td valign="top" align="center">
                                      <img style="border: 0px; display: block; width: 100%; " alt="Safegold" width="600" border="0" align="middle" title="Safegold" src="https://safegold.com.br/wp-content/themes/safeGold/assets/img/email-barra-endereco.jpg"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>