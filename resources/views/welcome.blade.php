@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'home', 'title' => __('Safegold'), 'titlePage' => __('Home')])

@section('content')
<div class="container" style="height: auto;">
    <div class="row justify-content-center">
        <div class="col-lg-7 col-md-8">
            <h1 class="text-white text-center">{{ __('Bem-vindo à administração de teasers da Safegold.') }}</h1>

            <br>
            <br>

            @include('shared.logo')
        </div>
    </div>
</div>
@endsection
