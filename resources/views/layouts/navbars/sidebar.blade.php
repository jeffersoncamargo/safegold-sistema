<div class="sidebar" data-color="orange" style="background: #2a2c29;">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo" style="width: 90%; margin: 0 auto;">
    <a href="{{ url('/') }}" class="simple-text logo-normal">
      @include('shared.logo')
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'users') ? ' active' : '' }}">
        <a class="nav-link {{ ($activePage == 'profile' || $activePage == 'users') ? '' : 'collapsed' }}" data-toggle="collapse" href="#laravelExample" aria-expanded="{{ ($activePage == 'profile' || $activePage == 'users') ? 'true' : 'false' }}">
          <i class="material-icons">perm_identity</i>
          <p>{{ __('Usuários') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse {{ ($activePage == 'profile' || $activePage == 'users') ? 'show' : '' }}" id="laravelExample">
          <ul class="nav">
            @can('index', App\Models\User::class)
              <li class="nav-item{{ $activePage == 'users' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('users.index') }}">
                  <i class="material-icons">supervisor_account</i>
                  <span class="sidebar-normal"> {{ __('Gerenciar usuários') }} </span>
                </a>
              </li>
            @endcan
            <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('profile.edit') }}">
                <i class="material-icons">assignment_ind</i>
                <span class="sidebar-normal">{{ __('Meu perfil') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item{{ $activePage == 'segments' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('segments.index') }}">
          <i class="material-icons">segment</i>
            <p>{{ __('Segmentos') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'companies' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('companies.index') }}">
          <i class="material-icons">work</i>
            <p>{{ __('Empresas') }}</p>
        </a>
      </li>
      {{-- <li class="nav-item{{ $activePage == 'customers' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('customers.index') }}">
          <i class="material-icons">fingerprint</i>
            <p>{{ __('Clientes') }}</p>
        </a>
      </li> --}}
      <li class="nav-item {{ ($activePage == 'customers' || $activePage == 'approbation') ? ' active' : '' }}">
        <a class="nav-link {{ ($activePage == 'customers' || $activePage == 'approbation') ? '' : 'collapsed' }}" data-toggle="collapse" href="#customersCollapse" aria-expanded="{{ ($activePage == 'customers' || $activePage == 'approbation') ? 'true' : 'false' }}">
          <i class="material-icons">fingerprint</i>
          <p>{{ __('Clientes') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse {{ ($activePage == 'customers' || $activePage == 'approbation') ? 'show' : '' }}" id="customersCollapse">
          <ul class="nav">
            @can('index', App\Models\Customer::class)
              <li class="nav-item{{ $activePage == 'customers' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('customers.index') }}">
                  <i class="material-icons">supervisor_account</i>
                  <span class="sidebar-normal"> Todos os clientes </span>
                </a>
              </li>
            @endcan
            <li class="nav-item{{ $activePage == 'approbation' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('customers.approving-list') }}">
                <i class="material-icons">thumbs_up_down</i>
                <span class="sidebar-normal">{{ __('Pendentes de aprovação') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
          <i class="material-icons">exit_to_app</i>
          <p>{{ __('Sair') }}</p>
        </a>
      </li>
    </ul>
  </div>
</div>