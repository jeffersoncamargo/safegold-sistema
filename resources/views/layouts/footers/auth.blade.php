<footer class="footer">
    <div class="container-fluid">
        <nav class="float-left">
            <ul>
                <li>
                    <a href="https://www.safegold.com.br">
                        {{ __('Site institucional') }}
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>, desenvolvido por
            <a href="https://www.paandadesign.com" target="_blank" style="color: #eba904">Paanda Design</a>.
        </div>
    </div>
</footer>