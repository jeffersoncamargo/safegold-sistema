@extends('layouts.app', ['activePage' => 'segments', 'titlePage' => __('Editar segmento')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="{{ route('segments.update', $segment->id) }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    
                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ $segment->name }}</h4>
                            <p class="card-category">{{ __('Informações do segmento') }}</p>
                        </div>
                        <div class="card-body ">
                            @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Nome') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nome') }}" value="{{ old('name', $segment->name) }}" required="true" aria-required="true"/>
                                        @if ($errors->has('name'))
                                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Imagem') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('file') ? ' has-danger' : '' }}">
                                        <input class="form-control" name="file" id="input-file" type="file" value="{{ old('file', null) }}"  style="position: relative; opacity: 1; z-index: 0;" placeholder="Selecione o arquivo" />
                                        <span id="" class="error text-info" for="input-file">Dimensões: 553 x 322 px</span>
                                        <br>
                                        @if ($errors->has('file'))
                                            <span id="file-error" class="error text-danger" for="input-file">{{ $errors->first('file') }}</span>
                                        @endif
                                        @if(file_exists(storage_path('app/public/segments/' . $segment->id)))
                                            <img src="{{ url('storage/segments/' . $segment->id) }}" alt="">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" name="active" value="0">
                                            <input type="checkbox" name="active" value="1" {{ old('active', $segment->active) ? 'checked' : '' }}> Este segmento está ativo?
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">{{ __('Salvar') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection