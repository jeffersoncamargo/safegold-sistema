@extends('layouts.app', ['activePage' => 'segments', 'titlePage' => __('Gerenciar segmentos')])

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Segmentos</h4>
                        <p class="card-category"> Gerenciamento de segmentos aos quais pertencem as empresas dos teasers</p>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @can('create', App\Models\Segment::class)
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{ route('segments.create') }}" class="btn btn-sm btn-primary">Novo segmento</a>
                                </div>
                            </div>
                        @endcan
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>
                                            Nome
                                        </th>
                                        <th>
                                            Ativo
                                        </th>
                                        <th class="text-right">
                                            Ações
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($segments as $segment)
                                        <tr>
                                            <td>
                                                {{ $segment->name }}
                                            </td>
                                            <td>
                                                {{ $segment->active ? 'Sim' : 'Não' }}
                                            </td>
                                            <td class="td-actions text-right">
                                                <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('segments.edit', $segment->id) }}" data-original-title="editar" title="editar">
                                                    <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-12 text-right">
                                {{ $segments->appends(request()->except(['page','_token']))->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @endsection