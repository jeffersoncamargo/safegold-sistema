<?php

namespace App\Models;

use Carbon\Carbon;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use HasApiTokens;
    
    protected $fillable = [
        'company_id',
        'registering_user_id',
        'approving_user_id',
        'cpf_cnpj',
        'cpf_cnpj_hash',
        'cpf_cnpj_mask',
        'password',
        'name',
        'email',
        'phone',
        'indication',
        'ip',
        'browser',
        'latitude',
        'longitude',
        'active',
        'registration_at',
        'approbation_at',
        'valid_until',
        'repprovation_user_id',
        'repprobed_at',
    ];

    public $dates = [
        'created_at',
        'updated_at',
        'registration_at',
        'approbation_at',
        'repprobation_at',
        'valid_until',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function registeringUser()
    {
        return $this->belongsTo(User::class, 'registering_user_id');
    }

    public function approvingUser()
    {
        return $this->belongsTo(User::class, 'approving_user_id');
    }

    public function repprovingUser()
    {
        return $this->belongsTo(User::class, 'repproving_user_id');
    }

    public function accesses()
    {
        return $this->hasMany(Access::class);
    }

    public function setRegistrationAtAttribute($value)
    {
        if ($value instanceof Carbon) {
            $this->attributes['registration_at'] = $value;
        } else if (is_string($value)) {
            $this->attributes['registration_at'] = Carbon::createFromFormat('d/m/Y H:i', $value);
        } else {
            $this->attributes['registration_at'] = null;
        }
    }

    public function setApprobationAtAttribute($value)
    {
        if ($value instanceof Carbon) {
            $this->attributes['approbation_at'] = $value;
        } else if (is_string($value)) {
            $this->attributes['approbation_at'] = Carbon::createFromFormat('d/m/Y H:i', $value);
        } else {
            $this->attributes['approbation_at'] = null;
        }
    }

    public function setRepprobationAtAttribute($value)
    {
        if ($value instanceof Carbon) {
            $this->attributes['repprobation_at'] = $value;
        } else if (is_string($value)) {
            $this->attributes['repprobation_at'] = Carbon::createFromFormat('d/m/Y H:i', $value);
        } else {
            $this->attributes['repprobation_at'] = null;
        }
    }

    public function setValidUntilAttribute($value)
    {
        if ($value instanceof Carbon) {
            $this->attributes['valid_until'] = $value;
        } else if (is_string($value)) {
            $this->attributes['valid_until'] = Carbon::createFromFormat('d/m/Y', $value);
        } else {
            $this->attributes['valid_until'] = null;
        }
    }

    public function fmtRegistrationAt()
    {
        return $this->registration_at ? $this->registration_at->format('d/m/Y H:i') : '';
    }

    public function fmtApprobationAt()
    {
        return $this->approbation_at ? $this->approbation_at->format('d/m/Y H:i') : '';
    }

    public function fmtRepprobationAt()
    {
        return $this->repprobation_at ? $this->repprobation_at->format('d/m/Y H:i') : '';
    }

    public function fmtValidUntil()
    {
        return $this->valid_until ? $this->valid_until->format('d/m/Y') : '';
    }

}
