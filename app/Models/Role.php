<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN = 1;
    const MANAGER = 2;
    const CONSULTOR = 3;

    protected $fillable = [
        'name',
    ];

    public $timestamps = null;

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
