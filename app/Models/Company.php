<?php

namespace App\Models;

use App\Models\Traits\HasImage;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasImage;

    protected $fillable = [
        'segment_id',
        'earning_range_id',
        'code',
        'name',
        'employeees',
        'state',
        'city',
        'cropinfo',
        'active',
    ];

    public function segment()
    {
        return $this->belongsTo(Segment::class);
    }
    
    public function earningRange()
    {
        return $this->belongsTo(EarningRange::class);
    }

    public static function forApi($search = [])
    {
        $data = [];

        $builder = self::where('active', true)->orderBy('name');

        if (isset($search['segment_id']) && $search['segment_id']) {
            $builder->where('segment_id', $search['segment_id']);
        }

        if (isset($search['earning_range_id']) && $search['earning_range_id']) {
            $builder->where('earning_range_id', $search['earning_range_id']);
        }
        
        if (isset($search['state']) && $search['state']) {
            $builder->whereIn('state', $search['state']);
        }

        if (isset($search['code']) && $search['code']) {
            $builder->where('code', 'like', "%{$search['code']}%");
        }

        foreach ($builder->paginate(6) as $company) {
            $data[] = [
                'id' => $company->id,
                'code' => $company->code,
                'name' => $company->name,
                'state' => $company->state,
                'city' => $company->city,
                'segment' => $company->segment->name,
                'image' => url("storage/segments/{$company->segment->id}"),
            ];
        }

        return $data;
    }
}
