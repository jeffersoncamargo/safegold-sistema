<?php

namespace App\Models;

use App\Models\Traits\HasImage;
use Illuminate\Database\Eloquent\Model;

class Segment extends Model
{
    use HasImage;

    protected $fillable = [
        'name',
        'cropinfo',
        'active',
    ];

    public function companies()
    {
        return $this->hasMany(Company::class);
    }

    public static function forApi()
    {
        $data = [];

        $segments = self::where('active', true)
            ->whereHas('companies', function ($query) {
                $query->where('active', true);
            })
            ->orderBy('name')
            ->get();

        foreach ($segments as $segment) {
            $data[] = [
                'id' => $segment->id,
                'name' => $segment->name,
            ];
        }

        return $data;
    }
}
