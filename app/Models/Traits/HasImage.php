<?php

namespace App\Models\Traits;

use Intervention\Image\ImageManagerStatic as Image;

trait HasImage
{
    protected $filename;

    protected $srcfile;

    protected $oldImageName;

    public function imageName()
    {
        return ($this->id) ? ($this->id) : null;
    }

    public function originalImage()
    {
        $image_name = $this->imageName();
        if (!$image_name) {
            return null;
        }
        $image_path = "files/original/{$this->getTable()}/$image_name";
        if (file_exists($image_path)) {
            return url($image_path);
        }
        exec("find files/original/{$this->getTable()}/{$this->id}-*", $output);
        if(isset($output[0]) && $output[0]) {
            $image_path = $output[0];
            if (file_exists($image_path)) {
                return url($image_path);
            }
        }
        return null;
    }

    public function croppedImage()
    {
        $image_name = $this->imageName();
        if (!$image_name) {
            return null;
        }
        $image_path = "files/cropped/{$this->getTable()}/$image_name";
        if (file_exists($image_path)) {
            return url($image_path);
        }
        exec("find files/cropped/{$this->getTable()}/{$this->id}-*", $output);
        if(isset($output[0]) && $output[0]) {
            $image_path = $output[0];
            if (file_exists($image_path)) {
                return url($image_path);
            }
        }
        return url("/images/{$this->getTable()}.png");
    }

    public function thumbImage()
    {
        $image_name = $this->imageName();
        if (!$image_name) {
            return null;
        }
        $image_path = "files/thumb/{$this->getTable()}/$image_name";
        if (file_exists($image_path)) {
            return url($image_path);
        }
        exec("find files/thumb/{$this->getTable()}/{$this->id}-*", $output);
        if(isset($output[0]) && $output[0]) {
            $image_path = $output[0];
            if (file_exists($image_path)) {
                return url($image_path);
            }
        }
        return url("/images/{$this->getTable()}.png");
    }

    protected static function bootHasImage()
    {
        static::saving(function ($model)
        {
            if ($model->filename) {
                $model->srcfile = public_path("files/temp/{$model->filename}");
            } else {
                $table = $model->getTable();
                $image_name = $model->imageName();
                if ($image_name) {
                    $model->srcfile = public_path("files/original/$table/$image_name");
                }
            }
            $model->oldImageName = $model->imageName();

            if (!request()->input('cropinfo')) {
                $table = $model->getTable();
                $image_name = $model->imageName();
                if ($image_name) {
                    $original_file = public_path("files/original/$table/$image_name");
                    if (file_exists($original_file)) {
                        unlink($original_file);
                    }
                    $cropped_file = public_path("files/cropped/$table/$image_name");
                    if (file_exists($cropped_file)) {
                        unlink($cropped_file);
                    }
                }
                $model->cropinfo = null;
            }
        });

        static::saved(function ($model)
        {
            if (!file_exists($model->srcfile)) {
                return;
            }

            $image = Image::make($model->srcfile);
            $crop = json_decode($model->cropinfo);
            $image->crop((int) $crop->w, (int) $crop->h, (int) $crop->x, (int) $crop->y);
            $table = $model->getTable();
            $width = config("gluck.images.$table.width");
            $height = config("gluck.images.$table.height");

			$orientation = ($image->width() > $image->height() ? 'l' : 'p');
			$cropVal['max'] = ($width > $height ? $width : $height);
			$cropVal['min'] = ($width < $height ? $width : $height);

			if($orientation == 'l'){
				if ($image->height() > $cropVal['min']) {
					$image->resize(null, $cropVal['min'], function ($constraint) {
						$constraint->aspectRatio();
					});
				}
			}elseif($orientation == 'p'){
				if ($image->width() > $cropVal['min']) {
					$image->resize($cropVal['min'], null, function ($constraint) {
						$constraint->aspectRatio();
					});
				}
			}

            $width = $image->width() > $width ? $width : $image->width();
            $height = $image->height() > $height ? $height : $image->height();

			$image->crop($width, $height, 0, 0);
            $image_name = $model->imageName();
            $image->save(public_path("files/cropped/$table/$image_name"));
			$image->crop($height, $height);
            $image->resize(100, 100);
            $image->save(public_path("files/thumb/$table/$image_name"));
            //
            rename($model->srcfile, public_path("files/original/$table/$image_name"));
            if ($model->oldImageName && $model->oldImageName != $image_name) {
                $old_original_file = public_path("files/original/$table/{$model->oldImageName}");
                if (file_exists($old_original_file)) {
                    unlink($old_original_file);
                }
                $old_cropped_file = public_path("files/cropped/$table/{$model->oldImageName}");
                if (file_exists($old_cropped_file)) {
                    unlink($old_cropped_file);
                }
                $old_thumb_file = public_path("files/thumb/$table/{$model->oldImageName}");
                if (file_exists($old_thumb_file)) {
                    unlink($old_thumb_file);
                }
            }
        });

        static::deleted(function ($model)
        {
            $table = $model->getTable();
            $image_name = $model->imageName();
            $original_file = public_path("files/original/$table/$image_name");
            if (file_exists($original_file)) {
                unlink($original_file);
            }
            $cropped_file = public_path("files/cropped/$table/$image_name");
            if (file_exists($cropped_file)) {
                unlink($cropped_file);
            }
        });
    }

    /**
     * Recria a imagem centralizada dentro de um canvas com as dimensões finais, na maior resolução possível,
     * e então estabelece o crop para a área total da imagem.
     *
     * Este método deverá ser utilizado pela rotina de importação das imagens do sistema antigo,
     * sendo que não haverá definição do crop realizada por uma pessoa.
     */
    public function autocrop()
    {
        $srcfile = public_path("files/temp/{$this->filename}");
        if (!file_exists($srcfile)) {
            return;
        }
		$image = Image::make($srcfile);
        $table = $this->getTable();
        $width = $image->width();
        $height = $image->height();

        $this->cropinfo = json_encode([
            'w' => $width,
            'h' => $height,
            'x' => 0,
            'y' => 0,
        ]);
    }

    public function fill(array $attributes)
    {
        if (isset($attributes['filename'])) {
            $this->filename = $attributes['filename'];
        }
        parent::fill($attributes);
    }
}