<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EarningRange extends Model
{
    protected $fillable = [
        'min',
        'max',
    ];

    public function companies()
    {
        return $this->hasMany(Company::class);
    }

    public static function forSelect()
    {
        $options = [];

        foreach (self::get() as $range) {
            $options[$range->id] = "De R$ " . number_format($range->min, 2, ',', '.') . " a R$ " . number_format($range->max, 2, ',', '.');
        }

        return $options;
    }
}
