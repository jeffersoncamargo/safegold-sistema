<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\Segment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SegmentPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function index(User $user)
    {
        return $user->role_id < Role::CONSULTOR;
    }

    public function create(User $user)
    {
        return $user->role_id < Role::CONSULTOR;
    }

    public function store(User $user, Segment $obj)
    {
        return $user->role_id < Role::CONSULTOR;
    }

    public function edit(User $user, Segment $obj)
    {
        return $user->role_id < Role::CONSULTOR;
    }

    public function update(User $user, Segment $obj)
    {
        return $user->role_id < Role::CONSULTOR;
    }
}
