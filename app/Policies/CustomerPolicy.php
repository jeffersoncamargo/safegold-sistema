<?php

namespace App\Policies;

use App\Models\Customer;
use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CustomerPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function index(User $user)
    {
        return $user->role_id <= Role::CONSULTOR;
    }

    public function create(User $user)
    {
        return $user->role_id <= Role::CONSULTOR;
    }

    public function store(User $user, Customer $obj)
    {
        return $user->role_id <= Role::CONSULTOR;
    }

    public function edit(User $user, Customer $obj)
    {
        return $user->role_id <= Role::CONSULTOR;
    }

    public function update(User $user, Customer $obj)
    {
        return $user->role_id <= Role::CONSULTOR;
    }
}
