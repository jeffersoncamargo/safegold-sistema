<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function index(User $user)
    {
        return $user->role_id < Role::CONSULTOR;
    }

    public function create(User $user)
    {
        return $user->role_id < Role::CONSULTOR;
    }

    public function store(User $user, User $obj)
    {
        return $user->role_id < Role::CONSULTOR && $obj->role_id > $user->role_id;
    }

    public function edit(User $user, User $obj)
    {
        return $user->role_id < Role::CONSULTOR && $user->id != $obj->id;
    }

    public function update(User $user, User $obj)
    {
        return $user->role_id < Role::CONSULTOR && $user->id != $obj->id && $obj->role_id > $user->role_id;
    }
}
