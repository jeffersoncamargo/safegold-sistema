<?php

namespace App\Policies;

use App\Models\Company;
use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function index(User $user)
    {
        return $user->role_id <= Role::CONSULTOR;
    }

    public function create(User $user)
    {
        return $user->role_id <= Role::CONSULTOR;
    }

    public function store(User $user, Company $obj)
    {
        return $user->role_id <= Role::CONSULTOR;
    }

    public function edit(User $user, Company $obj)
    {
        return $user->role_id <= Role::CONSULTOR;
    }

    public function update(User $user, Company $obj)
    {
        return $user->role_id <= Role::CONSULTOR;
    }
}
