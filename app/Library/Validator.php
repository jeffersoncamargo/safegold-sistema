<?php

namespace App\Library;

use App\Library\Validate;
use Illuminate\Validation\Validator as BaseValidator;

/**
 * Bridge between Laravel and the effective Validate class.
 */
class Validator extends BaseValidator
{
    public function validateCpf($attribute, $value, $parameters)
    {
        return Validate::cpf($value);
    }

    public function validateCnpj($attribute, $value, $parameters)
    {
        return Validate::cnpj($value);
    }

    public function validateCpfCnpj($attribute, $value, $parameters)
    {
        return Validate::cpfCnpj($value);
    }

    public function validateAllowedCnpj($attribute, $value, $parameters)
    {
        return Validate::allowedCnpj($value);
    }
}
