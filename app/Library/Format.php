<?php

namespace App\Library;

use Carbon\Carbon;

class Format
{
    private static $days = [
        0 => 'domingo',
        1 => 'segunda-feira',
        2 => 'terça-feira',
        3 => 'quarta-feira',
        4 => 'quinta-feira',
        5 => 'sexta-feira',
        6 => 'sábado',
    ];

    public static function cpf($value)
    {
        return self::mask(str_pad($value, 11, '0', STR_PAD_LEFT), '###.###.###-##');
    }

    public static function cnpj($value)
    {
        return self::mask(str_pad($value, 14, '0', STR_PAD_LEFT), '##.###.###/####-##');
    }

    public static function zipcode($value)
    {
        return self::mask(str_pad($value, 8, '0', STR_PAD_LEFT), '##.###-###');
    }

    public static function phone($value)
    {
        if(strlen($value) > 10) {
            return self::mask($value, '(##) #####-####');
        } else {
            return self::mask(str_pad($value, 10, '0', STR_PAD_LEFT), '(##) ####-####');
        }
    }

    public static function money($value, $show_symbol = false)
    {
        $symbol = ($show_symbol) ? 'R$ ' : '';
        return $symbol . number_format($value, 2, ',', '.');
    }

    public static function integer($value)
    {
        return number_format($value, 0, ',', '.');
    }

    public static function kg($value, $show_symbol = true)
    {
        $symbol = ($show_symbol) ? ' Kg' : '';
        return number_format($value, 3, ',', '.') . $symbol;
    }

    public static function cm($value, $show_symbol = true)
    {
        $symbol = ($show_symbol) ? ' cm' : '';
        return number_format($value, 1, ',', '.') . $symbol;
    }

    public static function mask($value, $mask)
    {
        $result = '';
        $k = 0;
        for ($i = 0; $i < strlen($mask); $i++) {
            if ($mask[$i] == '#') {
                if (isset($value[$k])) {
                    $result .= $value[$k++];
                }
            } else {
                if (isset($mask[$i])) {
                    $result .= $mask[$i];
                }
            }
        }
        return $result;
    }

    public static function dayOfWeek(Carbon $date)
    {
        if (!$date) {
            return '';
        }
        return self::$days[$date->dayOfWeek];
    }

    public static function strToCarbon($strdate, $suffix = '00:00:00')
    {
        if($suffix) {
            $suffix = ' ' . $suffix;
        }

        return Carbon::createFromFormat('d/m/Y', $strdate)->format("Y-m-d{$suffix}");
    }
}
