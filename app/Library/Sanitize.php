<?php

namespace App\Library;

class Sanitize
{
    public static function defaults($data, $convertBlankToNull = true)
    {
        foreach ($data as $key => $value) {
            // $data[$key] = filter_var(trim($value), FILTER_SANITIZE_STRING);
            if (is_array($value)) {
                $value = self::defaults($value);
                continue;
            }
            $data[$key] = filter_var(trim($value));
            if ($convertBlankToNull && $data[$key] === '') {
                $data[$key] = null;
            }
        }
        return $data;
    }

    public static function digits($value)
    {
        return preg_replace('/\D/', '', $value);
    }

    public static function alpha($value)
    {
        return preg_replace('/[^A-Za-z0-9]/', '', $value);
    }

    public static function stripSpaces($value)
    {
        return preg_replace('/\s+/', '', $value);
    }

    public static function float($value)
    {
        $result = preg_replace('/\.+/', '', $value);
        return (float) preg_replace('/\,+/', '.', $result);
    }

    public static function string($value)
    {
        return substr($value, 0, 255);
    }

    public static function text($value)
    {
        return substr($value, 0, 2000);
    }
}
