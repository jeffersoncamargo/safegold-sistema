<?php

namespace App\Library;

use Illuminate\Support\HtmlString;

class Form
{
    public static function selectWithCompany($name, $list, $selected = null, $options = array())
    {
        $build = array();
        $build[] = sprintf(
            '<select name="%s" id="%s" class="%s">',
            $name,
            $name,
            $options['class']
        );

        if(isset($options['placeholder']) && $options['placeholder']) {
            $build[] = sprintf(
                '<option value="" data-company-id="">%s</option>',
                $options['placeholder']
            );
        }

        foreach ($list as $key => $value) {
            $build[] = sprintf(
                '<option value="%s" data-company-id="%s">%s</option>',
                $value->id,
                $value->company_id,
                $value->name
            );
        }

        $build[] = '</select>';

        return new HtmlString(join("\n", $build));
    }
}
