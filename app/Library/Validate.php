<?php

namespace App\Library;

use Auth;

class Validate
{
    public static function cpf($value)
    {
        $cpf = preg_replace('/[^\d]/', '', $value);
        $j = 0;
        for ($i = 0; $i < (strlen($cpf)); $i++) {
            if (is_numeric($cpf[$i])) {
                $num[$j] = $cpf[$i];
                $j++;
            }
        }
        if ($j == 0) {
            return false;
        }
        if (count($num) != 11) {
            return false;
        } else {
            for ($i = 0; $i < 10; $i++) {
                if ($num[0] == $i && $num[1] == $i && $num[2] == $i && $num[3] == $i && $num[4] == $i && $num[5] == $i && $num[6] == $i && $num[7] == $i && $num[8] == $i) {
                    return false;
                    break;
                }
            }
        }
        if (!isset($isCpfValid)) {
            $j = 10;
            for ($i = 0; $i < 9; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            } else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[9]) {
                return false;
            }
        }
        if (!isset($isCpfValid)) {
            $j = 11;
            for ($i = 0; $i < 10; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            } else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[10]) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public static function cnpj($value)
    {
        $cnpj = preg_replace('/[^\d]/', '', $value);
        $num = array();
        for ($i = 0; $i < (strlen($cnpj)); $i++) {
            $num[] = $cnpj[$i];
        }
        if (count($num) != 14) {
            return false;
        }
        if ($num[0] == 0 && $num[1] == 0 && $num[2] == 0
            && $num[3] == 0 && $num[4] == 0 && $num[5] == 0
            && $num[6] == 0 && $num[7] == 0 && $num[8] == 0
            && $num[9] == 0 && $num[10] == 0 && $num[11] == 0) {
            return false;
        } else {
            $j = 5;
            for ($i = 0; $i < 4; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $j = 9;
            for ($i = 4; $i < 12; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            } else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[12]) {
                return false;
            }
        }
        $j = 6;
        for ($i = 0; $i < 5; $i++) {
            $multiplica[$i] = $num[$i] * $j;
            $j--;
        }
        $soma = array_sum($multiplica);
        $j = 9;
        for ($i = 5; $i < 13; $i++) {
            $multiplica[$i] = $num[$i] * $j;
            $j--;
        }
        $soma = array_sum($multiplica);
        $resto = $soma % 11;
        if ($resto < 2) {
            $dg = 0;
        } else {
            $dg = 11 - $resto;
        }
        if ($dg != $num[13]) {
            return false;
        } else {
            return true;
        }
    }

    public static function cpfCnpj($value)
    {
        $number = preg_replace('/[^\d]/', '', $value);
        if (strlen($number) == 11) {
            return self::cpf($value);
        } else {
            return self::cnpj($value);
        }
    }

    public static function allowedCnpj($value)
    {
        return (Auth::user() && Auth::user()->isJrdSuperAdmin()) || !in_array($value, config('jrd.forbidden_cnpjs'));
    }
}