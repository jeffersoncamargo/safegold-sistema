<?php

namespace App\Library;

use App\Models\User;
use Carbon\Carbon;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Request;

class Log
{
    public static function add($type, $data)
    {
        // Remove alguns atributos comuns que não pode ou não valem a pena serem armazenados.
        unset($data['_method']);
        unset($data['_token']);
        unset($data['captcha']);
        unset($data['password']);
        unset($data['password_confirmation']);
        unset($data['save']);
        unset($data['token']);
        //
        $data = array_merge($data, ['ip' => Request::ip()]);
        $dir = storage_path() . '/logs/' . date('Y-m');
        if (!file_exists($dir)) {
            mkdir($dir);
        }
        $log = new Logger($type);
        $log->pushHandler(new StreamHandler("$dir/{$type}-" . date('Y-m-d') . '.log'));
        $log->addInfo("{$type}:", $data);
    }

    public static function get($date, $type)
    {
        $path = storage_path() . '/logs/' . $date->format('Y-m') . "/{$type}-" . $date->format('Y-m-d') . '.log';
        if (file_exists($path)) {
            return fopen($path, 'r');
        }
        return null;
    }

    public static function date($line)
    {
        $date = trim(preg_replace('/^\[([0-9]{4}-[0-9]{2}-[0-9]{2}\s[0-9]{2}:[0-9]{2}:[0-9]{2}).*/', '$1', $line));
        if($date) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $date);
        }
        return null;
    }

    public static function data($line, $action)
    {
        $line = preg_replace("/^\[.*?\]\s{$action}\.INFO:\s{$action}:\s/", '', $line);
        $line = preg_replace("/\s\[\]\s$/", '', $line);
        if($line) {
            return (array) json_decode($line);
        }
        return null;
    }

    public static function user($line, $action)
    {
        $data = self::data($line, $action);
        if($data) {
            return User::find(array_values($data)[0]);
        }
        return null;
    }

    public static function action($line, $action)
    {
        $data = self::data($line, $action);
        if($data) {
            $action = preg_replace('/\[.\]\s/', '', array_keys($data)[0]);
            switch($action) {
                case 'CREATED':
                    return 'Criação';
                    break;
                case 'UPDATED':
                    return 'Atualização';
                    break;
                case 'DELETED':
                    return 'Exclusão';
                    break;

            }
        }
        return null;
    }

    public static function id($line, $action)
    {
        $data = self::data($line, $action);
        if($data) {
            return $data['id'];
        }
        return null;
    }
}
