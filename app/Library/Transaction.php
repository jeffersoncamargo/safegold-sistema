<?php

namespace App\Library;

use DB;

class Transaction
{
    protected static $transactions = 0;

    public static function begin()
    {
        if(DB::transactionLevel() == 0) {
            DB::beginTransaction();
        }

        self::$transactions++;
    }

    public static function commit()
    {
        if(DB::transactionLevel() > 0) {
            DB::commit();
            self::$transactions = 0;
        }
    }

    public static function rollback()
    {
        if(DB::transactionLevel() > 1) {
            self::$transactions--;
        } else {
            DB::rollback();
            self::$transactions = 0;
        }
    }

    public static function count()
    {
        return self::$transactions;
    }
}
