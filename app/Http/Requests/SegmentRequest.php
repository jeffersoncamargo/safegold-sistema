<?php

namespace App\Http\Requests;

use App\Models\Segment;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SegmentRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'name' => [
                'required', 'min:3', Rule::unique('segments')->ignore($this->route()->segment ?? null)
            ],
            'file' => [
                $this->route()->segment ? 'nullable' : 'required', 'file', 'mimes:jpg,jpeg,png', 'dimensions:width=553,height=322',
            ]
        ];
    }
}