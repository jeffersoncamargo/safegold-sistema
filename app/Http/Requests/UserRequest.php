<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function validationData()
    {
        $data = $this->all();
        
        if(!$data['password']) {
            unset($data['password']);
            unset($data['password_confirmation']);
        }

        $this->replace($data);
        return $data;
    }

    public function rules()
    {
        return [
            'role_id' => [
                'required', 'integer', 'min:' . (request()->user()->role_id + 1)
            ],
            'name' => [
                'required', 'min:3'
            ],
            'email' => [
                'required', 'email', Rule::unique('users')->ignore($this->route('user') ?? null)
            ],
            'password' => [
                $this->route()->user ? 'nullable' : 'required', 'confirmed', 'min:6'
            ]
        ];
    }
}
