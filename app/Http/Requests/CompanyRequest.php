<?php

namespace App\Http\Requests;

use App\Models\Company;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'segment_id' => [
                'required',
            ],
            'earning_range_id' => [
                'required',
            ],
            'code' => [
                'required', 'min:3', Rule::unique('companies')->ignore($this->route()->company ?? null)
            ],
            'name' => [
                'required', 'min:3', Rule::unique('companies')->ignore($this->route()->company ?? null)
            ],
            'employeees' => [
                'required', 'integer', 'min:1',
            ],
            'state' => [
                'required',
            ],
            'city' => [
                'required',
            ],
            'file' => [
                'nullable', 'file', 'mimes:pdf'
            ],
        ];
    }
}