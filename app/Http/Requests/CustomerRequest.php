<?php

namespace App\Http\Requests;

use App\Models\Customer;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function validationData()
    {
        $data = $this->all();

        if (isset($data['cpf_cnpj']) && $data['cpf_cnpj']) {
            $data['cpf_cnpj'] = preg_replace('/[^0-9]/', '', $data['cpf_cnpj']);
        }
        if (isset($data['phone']) && $data['phone']) {
            $data['phone'] = preg_replace('/[^0-9]/', '', $data['phone']);
        }

        $this->replace($data);
        return $data;
    }

    public function rules()
    {
        return [
            'company_id' => [
                'required',
            ],
            'cpf_cnpj' => [
                $this->route()->customer ? 'nullable' : 'required', 'min:11', 'max:14'
            ],
            'name' => [
                'required', 'min:3',
            ],
            'email' => [
                'required', 'email',
            ],
            'phone' => [
                'required', 'min:10', 'max:11'
            ],
            'indication' => [
                'nullable', 'min:3'
            ],
        ];
    }
}