<?php

namespace App\Http\Controllers;

use App\Models\Segment;
use App\Http\Requests\SegmentRequest;
use Illuminate\Support\Facades\Hash;

class SegmentsController extends Controller
{
    public function index(Segment $model)
    {
        $this->authorize('index', Segment::class);

        return view('segments.index', ['segments' => $model->paginate(15)]);
    }

    public function create()
    {
        $this->authorize('create', Segment::class);

        return view('segments.create', ['segment' => new Segment(),]);
    }

    public function store(SegmentRequest $request, Segment $model)
    {
        $model->fill($request->all());
        $this->authorize('store', $model);
        $model->save();

        $request->file('file')->storeAs(
            'segments', $model->id, [
                'disk' => 'my_files',
            ]
        );

        return redirect(route('segments.index'))->withStatus(__('Segmento criado.'));
    }

    public function edit($id)
    {
        $model = Segment::findOrFail($id);
        $this->authorize('edit', $model);

        return view('segments.edit', ['segment' => $model,]);
    }

    public function update(SegmentRequest $request, $id)
    {
        $model = Segment::findOrFail($id);
        $model->fill($request->all());
        $this->authorize('update', $model);
        $model->save();

        if ($request->hasFile('file')) {
            $request->file('file')->storeAs(
                'public/segments', $model->id, [
                    'disk' => 'my_files',
                ]
            );
        }

        return redirect(route('segments.index'))->withStatus(__('Segmento alterado.'));
    }
}
