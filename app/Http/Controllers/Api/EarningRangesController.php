<?php

namespace App\Http\Controllers\Api;

use App\Models\EarningRange;

class EarningRangesController extends Controller {

    public function index()
    {
        return $this->sendResponse(EarningRange::forSelect(), 'List of ranges');
    }

}