<?php

namespace App\Http\Controllers\Api;

use App\Models\Customer;
use Illuminate\Support\Facades\Auth;

class CustomersController extends Controller {

    public function show()
    {
        $customer = Auth::guard('api')->user();

        if (!$customer) {
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised'], 401);
        }

        return $this->sendResponse([
            'data' => base64_encode(file_get_contents(storage_path('app/companies/' . $customer->company_id))),
        ], 'Teaser data');
    }

}