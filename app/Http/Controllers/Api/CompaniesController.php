<?php

namespace App\Http\Controllers\Api;

use App\Models\Company;
use Illuminate\Http\Request;

class CompaniesController extends Controller {

    public function index(Request $request)
    {
        return $this->sendResponse(Company::forApi($request->all()), 'List of segments');
    }

}