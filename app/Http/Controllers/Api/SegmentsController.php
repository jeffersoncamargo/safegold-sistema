<?php

namespace App\Http\Controllers\Api;

use App\Models\Segment;

class SegmentsController extends Controller {

    public function index()
    {
        return $this->sendResponse(Segment::forApi(), 'List of segments');
    }

}