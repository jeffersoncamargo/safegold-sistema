<?php

namespace App\Http\Controllers\Api;

use App\Mail\AccessData;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $data = $request->all();
        
        if (isset($data['cpf_cnpj']) && $data['cpf_cnpj']) {
            $data['cpf_cnpj'] = preg_replace('/[^0-9]/', '', $data['cpf_cnpj']);
        }
        if (isset($data['phone']) && $data['phone']) {
            $data['phone'] = preg_replace('/[^0-9]/', '', $data['phone']);
        }

        $request->replace($data);
        $validator = Validator::make($data, [
            'company_id' => [
                'required',
            ],
            'cpf_cnpj' => [
                'required', 'min:11', 'max:14'
            ],
            'name' => [
                'required', 'min:3',
            ],
            'email' => [
                'required', 'email',
            ],
            'phone' => [
                'required', 'min:10', 'max:11'
            ],
            'indication' => [
                'nullable', 'min:3'
            ],
        ]);

        if ($validator->fails()) {
            return $this->sendError('Erro de validação no servidor.', $validator->errors());       
        }

        $customer = Customer::where('email', $request->email)
            ->where('company_id', $request->company_id)
            ->whereNull('approbation_at')
            ->first();
        
        if ($customer) {
            return $this->sendError('Você já possui cadatro pendente de aprovação.', [], 401); 
        }

        $input = $request->all();
        $customer = new Customer();
        $customer->fill($input);

        $customer->cpf_cnpj_hash = Crypt::encrypt($request->cpf_cnpj);
        $customer->cpf_cnpj_mask = preg_replace('/([0-9]{3})([0-9]+)([0-9]{2})/', '\1' . (strlen($request->cpf_cnpj) == 11 ? '******' : '*********') . '\3', $request->cpf_cnpj);
        $customer->cpf_cnpj = bcrypt($request->cpf_cnpj);

        $customer->active = true;
        $customer->save();
        $success['name'] =  $customer->name;

        return $this->sendResponse($success, 'User register successfully.');
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $customer = Customer::where('email', $request->email)
            ->where('active', true)
            ->first();

        if ($customer && Hash::check($request->password, $customer->password)) { 
            if (!$customer->active || !$customer->approbation_at || $customer->valid_until->format('Ymd') < date('Ymd')) {
                return $this->sendError('Found.', ['error'=>'Found'], 401);
            }

            $success['token'] =  $customer->createToken('Safegold')->accessToken; 
            $success['name'] =  $customer->name;

            $customer->api_token = $success['token'];
            $customer->save();

            return $this->sendResponse($success, 'User login successfully.');
        } else { 
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised'], 401);
        } 
    }

    /**
     * Forgot Password
     *
     * @return \Illuminate\Http\Response
     */
    public function forgotPassword(Request $request)
    {
        $customer = Customer::where('email', $request->email)
            ->where('company_id', $request->company_id)
            ->where('active', true)
            ->whereNotNull('approbation_at')
            ->where('valid_until', '>=', date('Y-m-d'))
            ->first();

        if ($customer) { 
            $password = Str::random(8);
            $customer->password = bcrypt($password);
            $customer->save();

            Mail::to($customer)->send(new AccessData($customer, $password));

            return $this->sendResponse([], 'Paswword reseted.');
        } else { 
            return $this->sendError('Not Found.', ['error'=>'Not Found']);
        } 
    }
}