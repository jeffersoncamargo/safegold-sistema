<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Facades\Hash;

class CompaniesController extends Controller
{
    public function index(Company $model)
    {
        $this->authorize('index', Company::class);

        return view('companies.index', ['companies' => $model->paginate(15)]);
    }

    public function create()
    {
        $this->authorize('create', Company::class);

        return view('companies.create', ['company' => new Company(),]);
    }

    public function store(CompanyRequest $request, Company $model)
    {
        $model->fill($request->all());
        $this->authorize('store', $model);
        $model->save();

        $request->file('file')->storeAs(
            'companies', $model->id, [
                'disk' => 'local',
            ]
        );

        return redirect(route('companies.index'))->withStatus(__('Empresa criada.'));
    }

    public function edit($id)
    {
        $model = Company::findOrFail($id);
        $this->authorize('edit', $model);

        return view('companies.edit', ['company' => $model,]);
    }

    public function update(CompanyRequest $request, $id)
    {
        $model = Company::findOrFail($id);
        $model->fill($request->all());
        $this->authorize('update', $model);
        $model->save();

        if ($request->hasFile('file')) {
            $request->file('file')->storeAs(
                'companies', $model->id, [
                    'disk' => 'local',
                ]
            );
        }

        return redirect(route('companies.index'))->withStatus(__('Empresa alterada.'));
    }
}
