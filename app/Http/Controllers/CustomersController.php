<?php

namespace App\Http\Controllers;

use App\Mail\AccessData;
use App\Models\Customer;
use App\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class CustomersController extends Controller
{
    public function index(Customer $model)
    {
        $this->authorize('index', Customer::class);

        return view('customers.index', ['customers' => $model->paginate(15)]);
    }

    public function create()
    {
        $this->authorize('create', Customer::class);

        return view('customers.create', ['customer' => new Customer(),]);
    }

    public function store(CustomerRequest $request, Customer $model)
    {
        $model->fill($request->all());
        $this->authorize('store', $model);

        $model->cpf_cnpj_hash = Crypt::encrypt($request->cpf_cnpj);
        $model->cpf_cnpj_mask = preg_replace('/([0-9]{3})([0-9]+)([0-9]{2})/', '\1' . (strlen($request->cpf_cnpj) == 11 ? '******' : '*********') . '\3', $request->cpf_cnpj);
        $model->cpf_cnpj = bcrypt($request->cpf_cnpj);

        $model->registering_user_id = $request->user()->id;
        $model->approving_user_id = $request->user()->id;
        $model->registration_at = date('d/m/Y H:i');
        $model->approbation_at = date('d/m/Y H:i');
        $model->valid_until = date('d/m/Y', strtotime('+7 day'));
        $model->active = true;

        $password = Str::random(8);
        $model->password = bcrypt($password);

        $model->save();

        Mail::to($model)->send(new AccessData($model, $password));

        return redirect(route('customers.index'))->withStatus(__('Cliente criado.'));
    }

    public function edit($id)
    {
        $model = Customer::findOrFail($id);
        $this->authorize('edit', $model);

        return view('customers.edit', ['customer' => $model,]);
    }

    public function update(CustomerRequest $request, $id)
    {
        $model = Customer::findOrFail($id);
        $data = $request->all();
        $approving = false;

        if (isset($data['approved']) && $data['approved'] && !$model->approved_at) {
            $approving = true;
        }

        $model->fill($data);
        
        if ($approving) {
            $model->approving_user_id = $request->user()->id;
            $model->approbation_at = date('d/m/Y H:i');
            $model->valid_until = date('d/m/Y', strtotime('+7 day'));

            $password = Str::random(8);
            $model->password = bcrypt($password);
        }

        $this->authorize('update', $model);
        $model->save();

        if ($approving) {
            Mail::to($model)->send(new AccessData($model, $password));
        }

        return redirect(route('customers.index'))->withStatus(__('Cliente alterado.'));
    }

    public function approve(Request $request, $id)
    {
        $model = Customer::findOrFail($id);

        $model->approving_user_id = $request->user()->id;
        $model->approbation_at = date('d/m/Y H:i');
        $model->valid_until = date('d/m/Y', strtotime('+7 day'));

        $model->repproving_user_id = null;
        $model->repprobation_at = null;

        $model->save();

        Mail::to($model)->send(new AccessData($model, $password));

        return redirect(route('customers.index'))->withStatus(__('Cliente aprovado.'));
    }

    public function repprove(Request $request, $id)
    {
        $model = Customer::findOrFail($id);

        $model->approving_user_id = null;
        $model->approbation_at = null;
        $model->valid_until = null;

        $model->repproving_user_id = $request->user()->id;
        $model->repprobation_at = date('d/m/Y H:i');

        $model->save();

        return redirect(route('customers.index'))->withStatus(__('Cliente reprovado.'));
    }

    public function approvingList(Customer $model)
    {
        $this->authorize('index', Customer::class);

        return view('customers.approving-list', ['customers' => $model->whereNull('approbation_at')->whereNull('repprobation_at')->paginate(15)]);
    }
}
