<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index(User $model)
    {
        $this->authorize('index', User::class);

        return view('users.index', ['users' => $model->where('role_id', '>', request()->user()->role_id)->paginate(15)]);
    }

    public function create()
    {
        $this->authorize('create', User::class);

        return view('users.create', ['user' => new User(),]);
    }

    public function store(UserRequest $request, User $model)
    {
        $model->fill($request->all());
        $this->authorize('store', $model);
        $model->password = bcrypt($request->password);
        $model->save();

        return redirect(route('users.index'))->withStatus(__('Usuário criado.'));
    }

    public function edit($id)
    {
        $model = User::findOrFail($id);
        $this->authorize('edit', $model);

        return view('users.edit', ['user' => $model,]);
    }

    public function update(UserRequest $request, $id)
    {
        $model = User::findOrFail($id);
        $model->fill($request->all());
        $this->authorize('update', $model);
        if ($request->password) {
            $model->password = bcrypt($request->password);
        }
        $model->save();

        return redirect(route('users.index'))->withStatus(__('Usuário alterado.'));
    }
}
