<?php

namespace App\Mail;

use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccessData extends Mailable
{
    use Queueable, SerializesModels;

    protected $customer;
    protected $password;

    public function __construct(Customer $customer, string $password)
    {
        $this->customer = $customer;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('no-reply@safegold.com.br')
            ->replyTo('no-reply@safegold.com.br')
            ->view('emails.access-data')
            ->subject('Safegold | Dados de acesso a Teaser')
            ->with([
                'customer' => $this->customer,
                'password' => $this->password,
            ]);;
    }
}
