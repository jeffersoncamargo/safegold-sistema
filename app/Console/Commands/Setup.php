<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class Setup extends Command
{
    protected $signature = 'setup {--clear}';

    protected $description = 'Environment setup';

    private $acl = [
        'bootstrap/cache',
        'storage/app',
        'storage/app/public',
        'storage/app/public/segments',
        'storage/app/public/companies',
        'storage/framework',
        'storage/logs',
    ];

    private $clear = [
        'storage/debugbar',
        'storage/framework/cache',
        'storage/framework/sessions',
        'storage/framework/views',
        'storage/logs',
    ];

    public function handle()
    {
        $apache_user = $this->ask('What is the Apache username?', 'www-data');
        foreach ($this->acl as $dir) {
            if (!file_exists($dir)) {
                mkdir("./$dir", 0775, true);
                $process = Process::fromShellCommandline("setfacl -R -m u:$apache_user:rwX -m u:`whoami`:rwX $dir && setfacl -dR -m u:$apache_user:rwX -m u:`whoami`:rwX $dir");
                $process->run();
            }
            $this->comment("ACL changed for user $apache_user: $dir");
        }
        if ($this->option('clear')) {
            foreach ($this->clear as $dir) {
                $process = Process::fromShellCommandline("rm -fr $dir/*");
                $process->run();
                $this->comment("Directory cleared: $dir");
            }
        }
        $this->info("Environment setup finished!");
    }
}